# advent-of-code

A bunch of clojure functions specifically designed to solve advent-of-code challenges, daily.

Let's see how long this lasts.

## Usage

```
# Test everything (probably not recommended)
lein test
# Test everything in a year
lein test :2021
# Test a specific day in a year
lein test :only advent-of-code.2021/d3-test
```

## License

Distributed under the Eclipse Public License either version 1.0 or any later version.
