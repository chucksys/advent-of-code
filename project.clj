(defproject advent-of-code "0.1.0-SNAPSHOT"
  :description "Code snippets used in Advent of Code throughout the years"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :test-selectors {:2015 :2015
                   :2018 :2018
                   :2019 :2019
                   :2020 :2020
                   :2021 :2021}
  :main advent-of-code.main
  :dependencies [[org.clojure/clojure "1.11.0"]
                 [org.clojure/math.combinatorics "0.1.6"]
                 [ubergraph "0.8.2"]
                 [babashka/babashka.curl "0.1.1"]
                 [environ "1.2.0"]]
  :plugins [[cider/cider-nrepl "0.28.3"]
            [lein-eftest "0.5.9"]])
