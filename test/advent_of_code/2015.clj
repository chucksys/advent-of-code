(ns advent-of-code.2015
  (:require [clojure.test :refer :all]
            [advent-of-code.2015.d1 :as d1]
            [advent-of-code.2015.d2 :as d2]
            [advent-of-code.2015.d3 :as d3]
            [advent-of-code.2015.d4 :as d4]
            [advent-of-code.2015.d5 :as d5]
            [advent-of-code.2015.d6 :as d6]
            [advent-of-code.2015.d7 :as d7]
            [advent-of-code.2015.d8 :as d8]
            ))

(deftest ^:2015 ^:d1 d1-test
  (let [input (d1/clean "resources/input2015-d1")]
    (testing "Part 1"
      (is (= 74 (d1/to-what-floor input))))
    (testing "Part 2"
      (is (= 1795 (d1/basement-floor-pos input))))))

(deftest ^:2015 ^:d2 d2-test
  (let [input (d2/clean "resources/input2015-d2")]
    (testing "Part 1"
      (is (= 1598415 (d2/total-surface-area input))))
    (testing "Part 2"
      (is (= 3812909 (d2/total-ribbon-length input))))))

(deftest ^:2015 ^:d3 d3-test
  (let [input (d3/clean "resources/input2015-d3")]
    (testing "Part 1"
      (is (= 2592 (d3/count-houses-hit input))))
    (testing "Part 2"
      (is (= 2360 (d3/count-houses-hit2 input))))))

(deftest ^:2015 ^:d4 d4-test
  (let [secret "iwrupvqb"]
    (testing "Part 1"
      (is (= 346386 (d4/mine secret 346386 10489700))))
    (testing "Part 2"
      (is (= 9958218 (d4/mine2 secret 9958218 10489700))))))

(deftest ^:2015 ^:d5 d5-test
  (let [input (d5/clean)]
    (testing "Part 1"
      (is (= 255 (d5/count-nices input))))
    (testing "Part 2"
      (is (d5/no-overlapping-digraph-pairs? "xyxy"))
      (is (d5/no-overlapping-digraph-pairs? "aabcdefgaa"))
      (is (d5/no-overlapping-digraph-pairs? "aaaa"))
      (is (not (d5/no-overlapping-digraph-pairs? "aaa")))
      
      (is (= 55 (d5/count-nices2 input))))))

(deftest ^:2015 ^:d6 d6-test
  (let [input (d6/clean)
        ;lit (d6/setup-lights input)
        ]
    (testing "Part 1"
      (is (= 4 (count (d6/toggle-lights #{} {:sx 0 :sy 0 :ex 2 :ey 2}))))
      ;(is (= 377891 (count lit)))
      )
    (testing "Part 2"
      (is (= 1 (reduce + (vals (d6/setup-lights2 [{:instruction :turn-on :sx 0 :sy 0 :ex 1 :ey 1}])))))
      (is (= 0 (reduce + (vals (d6/setup-lights2 [{:instruction :turn-off :sx 0 :sy 0 :ex 1 :ey 1}])))))
      (is (= 2000000 (reduce + (vals (d6/setup-lights2 [{:instruction :toggle :sx 0 :sy 0 :ex 1000 :ey 1000}])))))
      ;(is (= 14110788 (reduce + (vals (d6/setup-lights2 input)))))
      )))

(deftest ^:2015 ^:d7 d7-test
  (let [input (d7/clean)]
    (testing "Part 1"
      (is (= 16076 (:a (d7/process-until input :a)))))
    (testing "Part 2"
      (is (= 2797 (:a (d7/process-until (d7/update-circuit input :b 16076) :a)))))))

(deftest ^:2015 ^:d8 d8-test
  (let [input (d8/clean)]
    (testing "Part 1"
      (is (= 1350 (d8/count-diff input))))
    (testing "Part 2"
      (is (= 2085 (d8/count-diff2 input))))))
