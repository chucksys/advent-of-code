(ns advent-of-code.2018
  (:require [clojure.test :refer :all]
            [advent-of-code.2018.d1 :as d1]
            [advent-of-code.2018.d2 :as d2]
            [advent-of-code.2018.d3 :as d3]
            [advent-of-code.2018.d4 :as d4]
            [advent-of-code.2018.d4 :as d5]
            [advent-of-code.2018.d4 :as d6]
            [advent-of-code.2018.d4 :as d7]))

(deftest ^:2018 d1-test
  (testing "Day 1 part 2"
    (let [fname "resources/input2018-d1"
          freqs (d1/get-freqs fname)
          twice (d1/reached-twice (cycle freqs) 0)]
      (is (= twice 549)))))

(deftest ^:2018 d2-test
  (let [fname "resources/input2018-d2"
        csum (d2/part1 fname)
        sames (apply str (first (d2/part2 fname)))]
    (testing "Part 1"
      (is (= csum 4920)))

    (testing "Part 2"
      (is (= sames "fonbwmjquwtapeyzikghtvdxl")))))

(deftest ^:2018 d3-test
  (let [fname "resources/input2018-d3"
        claims (d3/get-claims fname)
        cloth {}]
    (testing "Part 1"
      (is (= (d3/apply-claims claims cloth)
             120419)))

    (testing "Part 2"
      (is (= (:id (first (d3/find-unoverlappings claims cloth)))
             445)))))

;(deftest ^:2018 d4-test
;  (let [fname "resources/input2018-d4"
;        stats (d4/get-stats fname)
;        sleepmap (d4/add-minutes (d4/stats->sleepmap stats))
;        _ (println sleepmap)
;        guard (d4/get-max sleepmap)
;        minute (nth (reverse (sort-by second (second guard))) 2)]
;    (println minute)
;    (testing "Part 1"
;      (is (= 118599 (* (first guard) (first minute)))))))