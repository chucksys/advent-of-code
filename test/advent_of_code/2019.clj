(ns advent-of-code.2019
  (:require [clojure.test :refer :all]
            [ubergraph.core :as uber]
            [advent-of-code.2019.d2 :as d2]
            [advent-of-code.2019.d3 :as d3]
            [advent-of-code.2019.d4 :as d4]
            [advent-of-code.2019.d5 :as d5]
            [advent-of-code.2019.d6 :as d6]))

(deftest ^:2019 ^:d2 d2-test
  (let [input (d2/clean "resources/input2019-d2")
        input-short (d2/clean "resources/input2019-d2-easy")]
    (testing "Part 1"
      (is (= 3500 (d2/part1 input-short 9 10)))
      (is (= 3562624 (d2/part1 input 12 2))))

    (testing "Part 2"
      (is (= [82 98] (d2/part2 input 19690720))))))

(deftest ^:2019 ^:d3 d3-test
  (let [input (d3/clean "resources/input2019-d3")
        input-easy1 (d3/clean "resources/input2019-d3-easy1")
        input-easy2 (d3/clean "resources/input2019-d3-easy2")]
    (testing "Helper functions"
      (testing "Instruction processing"
        (is (= {:dir :R :ts 12} (d3/process-instr "R12")))
        (is (= [{:dir :U :ts 3}] (d3/process-instr-list "U3")))
        (is (= [{:dir :U :ts 3}
                {:dir :R :ts 12}]
               (d3/process-instr-list "U3,R12"))))

      (testing "Line creation and translation"
        (is (= '({:x 0 :y 1} {:x 0 :y 2} {:x 0 :y 3})
               (d3/get-nonoffset-coords {:dir :D :ts 3})))
        (is (= '({:x 0 :y 1} {:x 0 :y 2} {:x 0 :y 3})
               (d3/get-line-coords {:x 0 :y 0} {:dir :D :ts 3})))
        (is (= '({:x 3 :y 5} {:x 3 :y 6} {:x 3 :y 7})
               (d3/get-line-coords {:x 3 :y 4} {:dir :D :ts 3})))))

    (testing "Part 1"
      (is (= 159 (d3/part1 input-easy1)))
      (is (= 135 (d3/part1 input-easy2)))
      (is (= 386 (d3/part1 input))))

    (testing "Part 2"
      (is (= 610 (d3/part2 input-easy1)))
      (is (= 410 (d3/part2 input-easy2)))
      (is (= 6484 (d3/part2 input))))))

(deftest ^:2019 ^:d4 d4-test
  (let [input (range 134792 675811)]
    (testing "Validation functions"
      (is (d4/same-adj-digits? "122456"))
      (is (d4/same-adj-digits? "111111"))
      (is (not (d4/same-adj-digits? "123456")))

      (is (d4/never-decrease? "123456"))
      (is (not (d4/never-decrease? "123454")))

      (is (d4/valid-password? "111111"))
      (is (not (d4/valid-password? "223450")))
      (is (not (d4/valid-password? "123789")))

      (is (d4/wtf-adjacency? "112233"))
      (is (not (d4/wtf-adjacency? "123444")))
      (is (d4/wtf-adjacency? "111122")))

    (testing "Part 1"
      (is (= 1955 (d4/part1 input))))

    (testing "Part 2"
      (is (= 1319 (d4/part2 input))))))

(deftest ^:2019 ^:d5 d5-test
  (let [input (d5/clean "resources/input2019-d5")]
    (testing "Helper functions"
      (is (= [:pos :imm :pos 2] (d5/explode-opcode 1002))))
    
    (testing "Part 1"
      (is (= "0 0 0 0 0 0 0 0 0 13818007 " (with-out-str (with-in-str "1" (d5/part1 input))))))
    
    (testing "Part 2"
      (is (= "3176266 " (with-out-str (with-in-str "5" (d5/part2 input))))))))

(deftest ^:2019 ^:d6 d6-test
  (let [input1 (d6/clean1 "resources/input2019-d6")
        input2 (d6/clean2 "resources/input2019-d6")
        input-easy1 (d6/clean1 "resources/input2019-d6-easy")
        input-easy2 (d6/clean2 "resources/input2019-d6-easy-part2")]
    (testing "Total children"
      (let [g (uber/add-directed-edges (uber/graph) [:a :b] [:a :c] [:b :d])]
        (is (= 3 (d6/total-children g :a)))
        (is (= 1 (d6/total-children g :b)))
        (is (= 0 (d6/total-children g :c)))))

    (testing "Part 1"
      (is (= 42 (d6/part1 input-easy1)))
      (is (= 344238 (d6/part1 input1))))

    (testing "Part 2"
      (is (= 4 (d6/part2 input-easy2)))
      (is (= 436 (d6/part2 input2))))
    ))
