(ns advent-of-code.2021
  (:require [clojure.test :refer :all]
            [advent-of-code.utils :refer [resource-name]]
            [advent-of-code.2021.d1 :as d1]
            [advent-of-code.2021.d2 :as d2]
            [advent-of-code.2021.d3 :as d3]
            [advent-of-code.2021.d4 :as d4]
            [advent-of-code.2021.d5 :as d5]
            [advent-of-code.2021.d6 :as d6]
            [advent-of-code.2021.d7 :as d7]
            [advent-of-code.2021.d8 :as d8]))

(deftest ^:2021 ^:d1 d1-test
  (let [input (d1/clean (resource-name 2021 1))
        c (d1/count-increasing-pairs input)
        cc (d1/count-increasing-threesums input)]
    (testing "Part 1"
      (is (= c 1266)))
    (testing "Part 2"
      (is (= cc 1217)))))

(deftest ^:2021 ^:d2 d2-test
  (let [instructions (d2/clean (resource-name 2021 2))
        {:keys [hpos depth]} (d2/simulate-submarine instructions)]
    (testing "Part 2"
      (is (= (* hpos depth) 2086261056)))))

(deftest ^:2021 ^:d3 d3-test
  (let [report (d3/clean (resource-name 2021 3))]
    (testing "Part 1"
      (is (= (d3/submarine-power-consumption report) 741950)))
    (testing "Part 2"
      (is (= (d3/submarine-life-support-rating report) 903810)))))

(deftest ^:2021 ^:d4 d4-test
  (let [bingo-system (d4/clean (resource-name 2021 4))]
    (testing "Part 1"
      (is (= (d4/find-winning-board bingo-system) 41668)))
    (testing "Part 2"
      (is (= (d4/find-last-winning-board bingo-system) 10478)))))

(deftest ^:2021 ^:d5 d5-test
  (let [input (d5/clean (resource-name 2021 5))
        input2 (d5/clean2 (resource-name 2021 5))]
    (testing "Part 1"
      (is (= (d5/line->points {:x1 9 :y1 7 :orientation :horizontal :offset -2})
             '((9 7) (8 7) (7 7))))
      (is (= (d5/count-overlapping-points input) 5576)))
    (testing "Part 2"
      (is (= (d5/count-overlapping-points input2) 6000)))))

(deftest ^:2021 ^:d6 d6-test
  (let [input (d6/clean (resource-name 2021 6))]
    (testing "Part 1"
      (is (= (reduce + (vals (d6/simulate input 80)))
             386755)))
    (testing "Part 2"
      (is (= (reduce + (vals (d6/simulate input 256)))
             1732731810807)))))

(deftest ^:2021 ^:d7 d7-test
  (let [input (d7/clean (resource-name 2021 7))]
    (testing "Part 1"
      (is (= (d7/minimum-fuel-cost input) 352254)))
    (testing "Part 2"
      (is (= (d7/minimum-fuel-cost' input) 352254)))))

(deftest ^:2021 ^:d8 d8-test
  (let [input (d8/clean (resource-name 2021 8))]
    (testing "Part 1"
      (is (= (d8/count-output-uniq-reprs input) 456)))
    (testing "Part 2"
      (is (= (d8/decrypt-and-sum-outputs input) 1091609)))))
