(ns advent-of-code.2020
  (:require [clojure.test :refer :all]
            [clojure.set :refer [difference]]
            [advent-of-code.2020.d1 :as d1]
            [advent-of-code.2020.d2 :as d2]
            [advent-of-code.2020.d3 :as d3]
            [advent-of-code.2020.d4 :as d4]
            [advent-of-code.2020.d5 :as d5]
            [advent-of-code.2020.d6 :as d6]
            [advent-of-code.2020.d7 :as d7]
            [advent-of-code.2020.d8 :as d8]
            [advent-of-code.2020.d9 :as d9]
            [advent-of-code.2020.d10 :as d10]
            [advent-of-code.2020.d11 :as d11]))

(deftest ^:2020 ^:d1 d1-test
  (let [input (d1/clean "resources/input2020-d1")
        [x y] (d1/find-pair-for-sum input 2020)
        [a b c] (d1/find-npair-for-sum input 3 2020)]
    (testing "Part 1"
      (is (= (* x y) 494475)))
    (testing "Part 2"
      (is (= (* a b c) 267520550)))))

(deftest ^:2020 ^:d2 d2-test
  (let [input (d2/clean "resources/input2020-d2")
        num-valids (count (filter d2/valid? input))
        num-valids2 (count (filter d2/valid2? input))]
    (testing "Part 1"
      (is (> (count input) num-valids))
      (is (= num-valids 556)))
    (testing "Part 2"
      (is (> (count input) num-valids2))
      (is (= num-valids2 605)))))

(deftest ^:2020 ^:d3 d3-test
  (let [input (d3/clean "resources/input2020-d3")
        steps '((1 1) (3 1) (5 1) (7 1) (1 2))
        trees (map (partial d3/traverse input) steps)]
    (testing "Part 1"
      (is (= (first (first input)) false))
      (is (= (d3/traverse input '(3 1)) 289)))
    (testing "Part 2"
      (is (= (apply * trees) 5522401584)))))

(deftest ^:2020 ^:d4 d4-test
  (let [input (d4/clean "resources/input2020-d4")
        num-valids (count (filter d4/valid? input))
        num-valids2 (count (filter d4/valid2? input))]
    (testing "Part 1"
      (is (= num-valids 206)))
    (testing "Part 2"
      (is (= num-valids2 123)))))

(deftest ^:2020 ^:d5 d5-test
  (let [input (d5/clean "resources/input2020-d5")
        passes (map d5/process-pass input)
        sids (map :sid passes)]
    (testing "Utility functions"
      (is (= (d5/clean-line "BFFFBBFRRR")
             '(B F F F B B F R R R)))
      (is (= (d5/process-pass (d5/clean-line "BFFFBBFRRR"))
             {:instructions (d5/clean-line "BFFFBBFRRR")
              :row 70 :col 7 :sid 567}))
      (is (= (d5/process-pass (d5/clean-line "FFFBBBFRRR"))
             {:instructions (d5/clean-line "FFFBBBFRRR")
              :row 14 :col 7 :sid 119}))
      (is (= (d5/process-pass (d5/clean-line "BBFFBBFRLL"))
             {:instructions (d5/clean-line "BBFFBBFRLL")
              :row 102 :col 4 :sid 820})))
    (testing "Part 1"
      (is (= (apply max sids)
             806)))
    (testing "Part 2"
      (let [smallest (apply min sids)
            largest (apply max sids)
            existing (set (range smallest (inc largest)))
            sids (set sids)
            missings (difference existing sids)]
        (is (= (count missings) 1))
        (is (= (first missings) 562))))))

(deftest ^:2020 ^:d6 d6-test
  (let [input (d6/clean "resources/input2020-d6")
        sum (apply + (map count input))
        input' (d6/clean' "resources/input2020-d6")
        sum' (apply + (map count input'))]
    (testing "Part 1"
      (is (= sum 6625)))
    (testing "Part 2"
      (is (= sum' 3360)))))

(deftest ^:2020 ^:d7 d7-test
  (let [input (d7/clean "resources/input2020-d7")
        bags (into {} input)]
    (testing "Utility functions"
      (is (= (count input) 594))
      (is (= (count (keys bags)) 594))
      (is (d7/can-hold? {"a" {"b" 1}} "b" "a"))
      (is (not (d7/can-hold? {"a" {"b" 2}} "c" "a")))
      (is (d7/can-hold? {"a" {"b" 3}
                         "b" {"c" 2}}
                        "c"
                        "a")))
    (testing "Part 1"
      (is (= (count (filter (partial d7/can-hold? bags "shiny gold bag") (keys bags))) 142))
      )
    (testing "Part 2"
      (is (= (d7/count-bags bags "shiny gold bag")
             10220)))))

(deftest ^:2020 ^:d8 d8-test
  (let [input (d8/clean "resources/input2020-d8")
        acc-before-loop (d8/interpret-until-already-seen input)]
    (testing "Part 1"
      (is (= acc-before-loop [true 1584])))
    (testing "Part 2"
      (is (= (d8/get-non-terminating-acc input)
             [false 920])))))

(deftest ^:2020 ^:d9 d9-test
  (let [input (d9/clean "resources/input2020-d9")
        invalid-num (d9/find-invalid-number input)
        window (d9/find-continguous-sum input invalid-num)
        small (apply min window)
        large (apply max window)]
    (testing "Part 1"
      (is (= invalid-num 22406676)))
    (testing "Part 2"
      (is (not (nil? window)))
      (is (= small 931988))
      (is (= large 2010399))
      (is (= (+ small large) 2942387)))))

(deftest ^:2020 ^:d10 d10-test
  (let [input (d10/clean "resources/input2020-d10")
        sample1 [0 1 4 5 6 7 10 11 12 15 16 19 22]
        res (d10/parse-differences input)
        s1-ways (d10/ways-to-do-it sample1)
        res-ways (d10/ways-to-do-it input)]
    (testing "Part 1"
      (is (= (* (:one res) (:three res)) 2516)))
    (testing "Part 2"
      (is (= 8 s1-ways))
      (is (= 296196766695424 res-ways)))))

(deftest ^:2020 d11-test
  (let [input (d11/clean "resources/input2020-d11")
        stable (d11/step-until-stable input)
        ez (d11/clean "resources/input2020-d11-ez")]
    (testing "Utility functions")
    (testing "Part 1"
      (is (= 2359 (get (d11/count-things stable) \# 0))))
    (testing "Part 2")))
