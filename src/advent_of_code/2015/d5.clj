(ns advent-of-code.2015.d5
  (:require [advent-of-code.utils :as utils]))

(defn clean
  []
  (utils/get-lines "resources/input2015-d5"))

(defn count-vowels
  [x]
  (utils/count-if (fn [c] (.contains "aeiou" (str c))) x))

(defn twice-in-a-row?
  [x]
  (loop [[a b & xs] x]
    (cond
      (= a b) true
      (empty? xs) false
      :else (recur (cons b xs)))))

(defn no-bad-diphthongs?
  [x]
  (and (not (.contains x "ab"))
       (not (.contains x "cd"))
       (not (.contains x "pq"))
       (not (.contains x "xy"))))

(defn nice?
  [x]
  (and (>= (count-vowels x) 3)
       (twice-in-a-row? x)
       (no-bad-diphthongs? x)))

(defn no-overlapping-digraph-pairs?
  [x]
  (let [all-pairs (partition 2 1 x)]
    (loop [[p & ps] all-pairs]
      (cond
        (empty? ps) false
        (.contains (rest ps) p) true
        :else (recur ps)))))

(defn repeating-letter-with-one-in-between?
  [x]
  (let [all-triples (partition 3 1 x)
        p? (fn [[a b c]] (= a c))]
    (not (nil? (some p? all-triples)))))

(defn nice2?
  [x]
  (and (no-overlapping-digraph-pairs? x)
       (repeating-letter-with-one-in-between? x)))

(defn count-nices
  [input]
  (utils/count-if nice? input))

(defn count-nices2
  [input]
  (utils/count-if nice2? input))
