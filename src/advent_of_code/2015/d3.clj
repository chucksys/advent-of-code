(ns advent-of-code.2015.d3
  (:require [advent-of-code.utils :as utils]))

(defn go-up
  [[x y]]
  [x (inc y)])

(defn go-down
  [[x y]]
  [x (dec y)])

(defn go-left
  [[x y]]
  [(dec x) y])

(defn go-right
  [[x y]]
  [(inc x) y])

(defn convert
  [x]
  ({\^ go-up
    \v go-down
    \< go-left
    \> go-right} x))

(defn clean
  [filename]
  (->> filename
       (slurp)
       (map convert)))

(defn- special-inc
  "Like inc, but treats nil as 0."
  [x]
  (if (nil? x)
    1
    (inc x)))

(defn count-houses-hit
  [input]
  (loop [houses-hit {}
         [x y :as current] [0 0]
         [d & ds] input]
    (if (empty? ds)
      (count (update houses-hit current special-inc))
      (recur (update houses-hit current special-inc)
             (d current)
             ds))))

(defn count-houses-hit2
  [input]
  (loop [houses-hit {}
         scurrent [0 0]
         rcurrent [0 0]
         [sd rd & ds] input]
    (case (count ds)
      0 (count (update (update houses-hit scurrent special-inc)
                       rcurrent
                       special-inc))
      1 (recur (update (update houses-hit scurrent special-inc)
                       rcurrent
                       special-inc)
               (sd scurrent)
               rcurrent
               ds)
      (recur (update (update houses-hit scurrent special-inc)
                     rcurrent
                     special-inc)
             (sd scurrent)
             (rd rcurrent)
             ds))))
