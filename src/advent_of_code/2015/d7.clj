(ns advent-of-code.2015.d7
  (:require [clojure.string :as string]
            [advent-of-code.utils :as utils]))

(defn unknown-case
  [line]
  (printf "Unknown case for line `%s`\n" line)
  {:type :unknown})

(defn parse-value
  "Parse value into either an integer or a keyword (symbol)"
  [value]
  (let [maybe-num (re-seq #"\d+" value)]
    (if (nil? maybe-num)
      (keyword value)
      (Integer/parseInt value))))

(defn line->assignment
  [[lhs _ rhs]]
  {:type :assignment
   :lvalue (parse-value lhs)
   :rvalue (parse-value rhs)})

(defn line->unop
  [[op s _ rhs]]
  {:type :unop
   :op (keyword (.toLowerCase op))
   :arg (parse-value s)
   :rvalue (parse-value rhs)})

(defn line->binop
  [[arg1 op arg2 _ rhs]]
  {:type :binop
   :op (keyword (.toLowerCase op))
   :arg1 (parse-value arg1)
   :arg2 (parse-value arg2)
   :rvalue (parse-value rhs)})

(defn cleanup-line
  [line]
  (let [s (string/split line #"\s+")]
    (case (count s)
      3 (line->assignment s)
      4 (line->unop s)
      5 (line->binop s)
      (unknown-case line))))

(defn clean
  []
  (map cleanup-line (utils/get-resource 2015 7)))

(defn process-assign-instruction
  [{:keys [lvalue rvalue]} wire-values]
  (assoc wire-values rvalue (get wire-values lvalue lvalue)))

(defn process-unop-instruction
  [{:keys [op arg rvalue]} wire-values]
  (case op
    :not (assoc wire-values rvalue (bit-xor (get wire-values arg arg) 0xffff))))

(defn process-binop-instruction
  [{:keys [op arg1 arg2 rvalue]} wire-values]
  (case op
    :and (assoc wire-values rvalue (bit-and (get wire-values arg1 arg1)
                                            (get wire-values arg2 arg2)))
    :or (assoc wire-values rvalue (bit-or (get wire-values arg1 arg1)
                                          (get wire-values arg2 arg2)))
    :lshift (assoc wire-values rvalue
                   (bit-and 0xffff
                            (bit-shift-left (get wire-values arg1 arg1)
                                            (get wire-values arg2 arg2))))
    :rshift (assoc wire-values rvalue
                   (bit-shift-right (get wire-values arg1 arg1)
                                    (get wire-values arg2 arg2)))))

(defn process-instruction
  [x wire-values]
  (case (:type x)
    :assignment (process-assign-instruction x wire-values)
    :unop (process-unop-instruction x wire-values)
    :binop (process-binop-instruction x wire-values)
    wire-values))

(defn can-process-assign?
  [{:keys [lvalue rvalue]} wire-values]
  (or (integer? lvalue)
      (contains? wire-values lvalue)))

(defn can-process-unop?
  [{:keys [arg]} wire-values]
  (or (integer? arg)
      (contains? wire-values arg)))

(defn can-process-binop?
  [{:keys [arg1 arg2]} wire-values]
  (and (or (integer? arg1)
           (contains? wire-values arg1))
       (or (integer? arg2)
           (contains? wire-values arg2))))

(defn can-process?
  [x wire-values]
  (case (:type x)
    :assignment (can-process-assign? x wire-values)
    :unop (can-process-unop? x wire-values)
    :binop (can-process-binop? x wire-values)
    false))

(defn process-once
  "Run through the input once and see if we can do anything. Return pair of the inputs that weren't processed and new wire values."
  [input wire-values]
  (loop [[x & xs] input
         unprocessed '()
         wire-values wire-values]
    (cond
      (nil? x) (list unprocessed wire-values)
      (can-process? x wire-values) (recur xs unprocessed (process-instruction x wire-values))
      :else (recur xs (cons x unprocessed) wire-values))))

(defn update-circuit
  [input rvalue-to-find new-lhs]
  (loop [[x & xs] input
         processed '()]
    (cond
      (nil? x) processed
      (= rvalue-to-find (:rvalue x)) (recur xs (cons {:type :assignment
                                                      :lvalue new-lhs
                                                      :rvalue rvalue-to-find}
                                                     processed))
      :else (recur xs (cons x processed)))))

(defn process-until
  "Simulate wires until a value appears in wire x"
  [input x]
  (let [max-looping-times (count input)]
    (loop [to-process input
           wire-values {}
           i 0]
      (cond
        (= i max-looping-times) wire-values
        (contains? wire-values x) wire-values
        :else
        (let [[unprocessed new-wire-values] (process-once to-process wire-values)]
          (recur unprocessed new-wire-values (inc i)))))))
