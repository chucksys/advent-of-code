(ns advent-of-code.2015.d8
  (:require [advent-of-code.utils :as utils]
            [clojure.string :as string]))

(defn clean
  []
  (utils/get-resource 2015 8))

(defn count-string-literal-chars
  [input]
  (->> (map count input)
       (reduce +)))

(defn print-passthrough
  [x]
  (print x " -> ")
  x)

(defn println-passthrough
  [x]
  (println x)
  x)

(defn count-string-memory-chars
  [input]
  (->> (map (comp
              count
              read-string
              (fn [s] (string/replace s #"\\x[0-9a-fA-F][0-9a-fA-F]" " ")))
            input)
       (reduce +)))

(defn count-diff
  [input]
  (- (count-string-literal-chars input)
     (count-string-memory-chars input)))

(defn encode-string
  [x]
  (->> (string/escape
         x
         {\\ "\\\\",
          \" "\\\""})
       (format "\"%s\"")))

(defn count-string-encode-chars
  [input]
  (->> input
       (map (comp count encode-string))
       (reduce +)))

(defn count-diff2
  [input]
  (- (count-string-encode-chars input)
     (count-string-literal-chars input)))
