(ns advent-of-code.2015.d1
  (:require [clojure.string :as s]))

(defn convert
  [x]
  (if (= x \()
    :go-up
    :go-down))

(defn clean
  [filename]
  (->> filename
       (slurp)
       (s/trim)
       (map convert)))

(defn to-what-floor
  [instructions]
  (loop [floor 1  ; Start on first floor (floor 0)
         [x & xs] instructions]
    (if (empty? xs)
      floor
      (case x
        :go-up (recur (inc floor) xs)
        :go-down (recur (dec floor) xs)))))

(defn basement-floor-pos
  [instructions]
  (loop [floor 1
         [x & xs] instructions
         it 0]
    (if (or (empty? xs) (zero? floor))
      it  ; remember this refers to the previous iteration
      (case x
        :go-up (recur (inc floor) xs (inc it))
        :go-down (recur (dec floor) xs (inc it))))))
