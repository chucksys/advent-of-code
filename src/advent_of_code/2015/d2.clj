(ns advent-of-code.2015.d2
  (:require [clojure.string :as s]))

(defn clean-line
  [line]
  (map #(Integer/parseInt %) (s/split line #"x")))

(defn volume
  [[x y z]]
  (* x y z))

(defn smallest-side
  [measurements]
  (take 2 (sort measurements)))

(defn smallest-side-area
  [measurements]
  (let [[a b] (smallest-side measurements)]
    (* a b)))

(defn surface-area
  [[x y z :as measurements]]
  (+ (* x y 2)
     (* y z 2)
     (* x z 2)
     (smallest-side-area measurements)))

(defn ribbon-length
  [measurements]
  (let [[a b] (smallest-side measurements)
        v (volume measurements)]
    (+ v a a b b)))

(defn clean
  [filename]
  (map clean-line (-> filename
                      (slurp)
                      (s/split #"\n+"))))

(defn total-surface-area
  [input]
  (reduce + (map surface-area input)))

(defn total-ribbon-length
  [input]
  (reduce + (map ribbon-length input)))
