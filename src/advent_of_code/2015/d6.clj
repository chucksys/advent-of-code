(ns advent-of-code.2015.d6
  (:require [advent-of-code.utils :as utils]
            [clojure.set :refer [union difference]]))

(defn- line->
  [s m line]
  (let [pattern (re-pattern (str s " ([0-9]+),([0-9]+) through ([0-9]+),([0-9]+)"))
        [[_ sx sy ex ey]] (re-seq pattern line)
        sx (Integer/parseInt sx)
        sy (Integer/parseInt sy)
        ex (inc (Integer/parseInt ex))    ; increase by 1 for (range) off by one issues
        ey (inc (Integer/parseInt ey))]   ; increase by 1 for (range) off by one issues
    {:instruction m
     :sx sx
     :sy sy
     :ex ex
     :ey ey}))

(defn- ->turn-on
  [line]
  (line-> "turn on" :turn-on line))

(defn- ->turn-off
  [line]
  (line-> "turn off" :turn-off line))

(defn- ->toggle
  [line]
  (line-> "toggle" :toggle line))

(defn cleanup-line
  [line]
  (cond
    (.startsWith line "turn on") (->turn-on line)
    (.startsWith line "turn off") (->turn-off line)
    (.startsWith line "toggle") (->toggle line)
    :else nil))

(defn clean
  []
  (map cleanup-line (utils/get-resource 2015 6)))

(defn- expand-instruction-range-row
  [sx ex y]
  (set (for [x (range sx ex)]
         [x y])))

(defn expand-instruction-range
  "Expand range of instructions into a list of x,y pairs"
  [{:keys [sx sy ex ey]}]
  (->> (range sy ey)
       (map (comp set (partial expand-instruction-range-row sx ex)))
       (apply union)))

(defn turn-on-lights
  [lights instr]
  (->> (expand-instruction-range instr)
       (union lights)))

(defn turn-on-lights2
  [lights instr]
  (as-> (expand-instruction-range instr) $
    (into '() $)
    (utils/update-lots lights $ inc)))

(defn turn-off-lights
  [lights instr]
  (->> (expand-instruction-range instr)
       (difference lights)))

(defn dec''
  [x]
  (if (zero? x)
    x
    (dec x)))

(defn turn-off-lights2
  [lights instr]
  (as-> (expand-instruction-range instr) $
    (into '() $)
    (utils/update-lots lights $ dec'')))

(defn toggle-lights
  [lights instr]
  (loop [[coord & xs] (into '() (expand-instruction-range instr))
         acc lights]
    (cond
      (nil? coord) acc
      (contains? acc coord) (recur xs (disj acc coord))
      :else (recur xs (conj acc coord)))))

(defn toggle-lights2
  [lights instr]
  (as-> (expand-instruction-range instr) $
    (into '() $)
    (utils/update-lots lights $ (partial + 2))))

(defn setup-lights
  [instructions]
  (loop [[x & xs] instructions
         turned-on-lights #{}]
    (case (:instruction x)
      :turn-on (recur xs (turn-on-lights turned-on-lights x))
      :turn-off (recur xs (turn-off-lights turned-on-lights x))
      :toggle (recur xs (toggle-lights turned-on-lights x))
      nil turned-on-lights
      )))

(defn setup-lights2
  [instructions]
  (loop [[x & xs] instructions
         turned-on-lights (zipmap (into '() (expand-instruction-range {:sx 0 :sy 0 :ex 1000 :ey 1000}))
                                  (repeat 0))]
    (case (:instruction x)
      :turn-on (recur xs (turn-on-lights2 turned-on-lights x))
      :turn-off (recur xs (turn-off-lights2 turned-on-lights x))
      :toggle (recur xs (toggle-lights2 turned-on-lights x))
      nil turned-on-lights
      )))
