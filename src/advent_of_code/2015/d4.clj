(ns advent-of-code.2015.d4
  (:import [java.security MessageDigest]))

(def MD5 (MessageDigest/getInstance "MD5"))

(defn hexify "Convert byte sequence to hex string" [coll]
  (let [hex [\0 \1 \2 \3 \4 \5 \6 \7 \8 \9 \a \b \c \d \e \f]]
    (letfn [(hexify-byte [b]
              (let [v (bit-and b 0xFF)]
                [(hex (bit-shift-right v 4)) (hex (bit-and v 0x0F))]))]
      (apply str (mapcat hexify-byte coll)))))

(defn md5sum
  [input]
  (->> (.getBytes input "UTF-8")
       (.digest MD5)
       (hexify)))

(defn try-mine
  [secret x]
  (md5sum (format "%s%d" secret x)))

(defn valid-mine-hex?
  [secret x]
  (.startsWith (try-mine secret x) "00000"))

(defn valid-mine-hex2?
  [secret x]
  (.startsWith (try-mine secret x) "000000"))

(defn mine
  [secret start limit]
  (loop [x start]
    (if (or (= x limit) (valid-mine-hex? secret x))
      x
      (recur (inc x)))))

(defn mine2
  [secret start limit]
  (loop [x start]
    (if (or (= x limit) (valid-mine-hex2? secret x))
      x
      (recur (inc x)))))
