(ns advent-of-code.2021.d2
  (:require [clojure.string :as st]))

(defn- line->instruction
  [line]
  (as-> line $
    (st/split $ #"\s+")
    (let [[x n] $]
      {:direction (keyword x)
       :units (Integer/parseInt n)})))

(defn clean
  [filename]
  (as-> filename $
      (slurp $)
      (st/split $ #"\n+")
      (map line->instruction $)))

(defn execute-instruction
  [{:keys [hpos depth aim]} {:keys [direction units]}]
  (case direction
    :forward {:hpos (+ hpos units) :depth (+ depth (* aim units)) :aim aim}
    :up {:hpos hpos :depth depth :aim (- aim units)}
    :down {:hpos hpos :depth depth :aim (+ aim units)}))

(defn simulate-submarine
  [instructions]
  
  (loop [pos {:hpos 0 :depth 0 :aim 0}
         [x & xs :as instructions] instructions]
    (if (empty? instructions)
      pos
      (recur (execute-instruction pos x)
             xs))))

