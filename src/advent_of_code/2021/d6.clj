(ns advent-of-code.2021.d6
  (:require [clojure.string :as st]))

(defn clean
  [filename]
  (as-> filename $
    (slurp $)
    (st/split $ #"[,\n]")
    (map #(Integer/parseInt %) $)
    (frequencies $)))

(def lanternfish-rebound 6)
(def lanternfish-rebound-baby 8)

(defn plus-with-nil
  [& xs]
  (apply + (map #(if (nil? %) 0 %) xs)))

(defn spawn-lanternfish
  [m num-zeroes]
  (update
    (update m lanternfish-rebound (partial plus-with-nil num-zeroes))
    lanternfish-rebound-baby
    (partial plus-with-nil num-zeroes)))

(defn simulate-a-day
  [state]
  (loop [[k & ks :as all-keys] (keys state)
         acc {}]
    (if (empty? all-keys)
      acc
      (if (zero? k)
        ; spawn new lanternfish
        (recur ks (spawn-lanternfish acc (state k)))
        ; subtract 1 from key and move everything
        (recur ks (update acc (dec k) (partial plus-with-nil (state k))))))))

(defn simulate
  [state days]
  (loop [days-remaining days
         curr-state state]
    (if (zero? days-remaining)
      curr-state
      (recur (dec days-remaining) (simulate-a-day curr-state)))))
