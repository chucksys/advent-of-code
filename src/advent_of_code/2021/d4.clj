(ns advent-of-code.2021.d4
  (:require [clojure.string :as st]
            [clojure.set :refer [subset? difference]]))

(defn line->numbers
  [line]
  (map #(Integer/parseInt %) (filter (complement empty?) (st/split line #"[ ,]+"))))

(defn line->bingo-board
  [current-board y line]
  (loop [acc current-board
         [item & xs :as l] line
         x 0]
    (if (empty? l)
      acc
      (recur (assoc acc (list x y) item)
             xs
             (inc x)))))

(defn lines->bingo-board
  [lines]
  (let [raw-board (map line->numbers lines)]
    (loop [acc {}
           y 0
           [row & xs :as all-board] raw-board]
      (if (empty? all-board)
        acc
        (recur (line->bingo-board acc y row)
               (inc y)
               xs)))))

(defn lines->bingo-boards
  [lines board-size]
  
  (assert (zero? (mod (count lines) board-size))
          (format "Bingo boards must be divisible by %d" board-size))
  
  (loop [remaining-lines lines
         acc []]
    (if (empty? remaining-lines)
      acc
      (let [raw-board (take 5 remaining-lines)
            remaining-lines' (drop 5 remaining-lines)]
        (recur remaining-lines' (conj acc (lines->bingo-board raw-board)))))))

(defn make-bingo-input
  [[inp & lines]]
  {:board-size 5
   :input (line->numbers inp)
   :boards (lines->bingo-boards lines 5)})

(defn clean
  [filename]
  (as-> (slurp filename) $
    (st/split $ #"\n+")
    (make-bingo-input $)))

(defn generate-row
  [y]
  (map #(list % y) (range 5)))

(defn generate-rows
  []
  (map generate-row (range 5)))

(defn generate-col
  [x]
  (map #(list x %) (range 5)))

(defn generate-cols
  []
  (map generate-col (range 5)))

(defn generate-diag
  []
  '((map list (range 5) (range 5))
    (map list (range 5) (reverse (range 5)))))

(defn generate-winning-coordinates
  []
  (concat (generate-rows)
          (generate-cols)
          (generate-diag)))

(def winning-fiver (generate-winning-coordinates))

(defn board-win?
  [board numbers-in-play]
  (loop [[x & xs :as coords] winning-fiver]
    (if (empty? coords)
      false
      (if (subset? (set (map board x)) numbers-in-play)
        true
        (recur xs)))))

(defn score-board
  [board numbers-in-play]
  (let [just-called (last numbers-in-play)
        nums-on-board (set (vals board))
        unmarked (difference nums-on-board (set numbers-in-play))]
    (* just-called
       (reduce + 0 unmarked))))

(defn find-winning-board
  [{:keys [input boards]}]
  (loop [nums-in-play-len 5]

    (assert (<= nums-in-play-len (count input)))

    (let [winners (filter #(board-win? % (set (take nums-in-play-len input))) boards)]
      (if (empty? winners)
        (recur (inc nums-in-play-len))
        (score-board (first winners) (take nums-in-play-len input))))))

(defn find-last-winning-board
  [{:keys [input boards]}]
  (loop [nums-in-play-len 5
         to-be-won-boards boards]

    (assert (<= nums-in-play-len (count input)))

    (let [not-wins-again (filter #(not (board-win? % (set (take nums-in-play-len input)))) to-be-won-boards)]
      (if (empty? not-wins-again)
        (score-board (first to-be-won-boards) (take nums-in-play-len input))
        (recur (inc nums-in-play-len) not-wins-again)))))
