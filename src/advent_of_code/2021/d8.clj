(ns advent-of-code.2021.d8
  (:require [clojure.string :as st]
            [clojure.pprint :refer [pprint]]
            [clojure.math.combinatorics :refer [combinations]]
            [clojure.set :refer [map-invert subset? union difference intersection]]))

(defn raw->processed
  [raw]
  (map set (st/split raw #"\s+")))

(defn line->entry
  [line]
  (let [[sig-patterns-raw outputs-raw] (st/split line #" \| ")]
    {:signal-patterns (raw->processed sig-patterns-raw)
     :outputs (raw->processed outputs-raw)}))

(defn clean
  [filename]
  (as-> (slurp filename) $
    (st/split $ #"\n+")
    (map line->entry $)))

; corresponds to 1, 4, 7, 8 respectively, on a 7-segment display
(def uniq-signal-counts
  {2 #{\c \f}
   4 #{\b \c \d \f}
   3 #{\a \c \f}
   7 #{\a \b \c \d \e \f \g}})

(def signal-counts->nums
  {2 1
   4 4
   3 7
   7 8})

; 0 = 6 segments (abcefg)
; 1 = 2 segments (cf)
; 2 = 5 segments (acdeg)
; 3 = 5 segments (acdfg)
; 4 = 4 segments (bcdf)
; 5 = 5 segments (abdfg)
; 6 = 6 segments (abdefg)
; 7 = 3 segments (acf)
; 8 = 7 segments (abcdefg)
; 9 = 6 segments (abcdfg)
(def num->seg
  {0 #{\a \b \c \e \f \g}
   1 #{\c \f}
   2 #{\a \c \d \e \g}
   3 #{\a \c \d \f \g}
   4 #{\b \c \d \f}
   5 #{\a \b \d \f \g}
   6 #{\a \b \d \e \f \g}
   7 #{\a \c \f}
   8 #{\a \b \c \d \e \f \g}
   9 #{\a \b \c \d \f \g}})

; Map number of segments to the set of numbers that could be possibly achieved with said number of
; segments.
(def numsegs->num-possibility
  (as-> num->seg $
    (into [] $)
    (group-by (comp count second) $)
    (update-vals $ (comp set (partial map first)))))

(defn count-if-in
  [lst to-count]
  (count (filter (partial contains? lst) to-count)))

(defn count-output-uniq-reprs
  [entries]
  (->> entries
       (map (comp (partial count-if-in uniq-signal-counts)
                  (partial map count)
                  :outputs))
       (reduce +)))

(def segments (set "abcdefg"))

; Create a solution space, where we map the segment to a set of possible other segments.
; We map the scrambled segments to the solution (where it is actually supposed to point).
(def initial-solution
  (into {} (map #(vec [% segments]) segments)))

(defn translate-to-proper-segs
  "Map randomized segments to actual segments. If we are still unsure, then don't do it for that segment."
  [possibilities segments]
  (->>
    (map possibilities segments)
    (filter #(= 1 (count %)))
    (map first)
    (set)))

(defn seed-init-solution
  [segs]
  (loop [[seg & rest-segs :as all-segs] segs
         acc {}]
    (if (empty? all-segs)
      acc
      (if (contains? uniq-signal-counts (count seg))
        (recur rest-segs (assoc acc (signal-counts->nums (count seg)) seg))
        (recur rest-segs acc)))))

(defn solve-five
  [solution seg]
  (cond
    (subset? (solution 1) seg) (assoc solution 3 seg)
    (= 1 (count (difference (solution 4) seg))) (assoc solution 5 seg)
    :else (assoc solution 2 seg)))

(defn solve-six
  [solution seg]
  (cond
    (subset? (solution 4) seg) (assoc solution 9 seg)
    (subset? (solution 1) seg) (assoc solution 0 seg)
    :else (assoc solution 6 seg)))

(defn solve-segment
  [solution seg]
  (case (count seg)
    2 solution
    3 solution
    4 solution
    5 (solve-five solution seg)
    6 (solve-six solution seg)
    7 solution))

; Part 2 assumption: the numbers 1, 4, 7, and 8 are present in all entries (because that's what everyone
; else assumes, which works???)

(defn solve-entry
  [{:keys [signal-patterns outputs]}]
  (let [uniq-segments (set (concat signal-patterns outputs))]
    (loop [[seg & segs :as all-segs] uniq-segments
           solution (seed-init-solution uniq-segments)]
      (cond
        (empty? all-segs) solution
        :else (recur segs (solve-segment solution seg))))))

(defn decrypt-output
  [{:keys [outputs] :as s}]
  (as-> (solve-entry s) $
        (map-invert $)
        (map $ outputs)
        (apply str $)
        (Integer/parseInt $)))

(defn decrypt-and-sum-outputs
  [inputs]
  (->> (map decrypt-output inputs)
       (reduce +)))
