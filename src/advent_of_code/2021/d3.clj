(ns advent-of-code.2021.d3
  (:require [clojure.string :as st]
            [clojure.pprint :refer [pprint]]))

(defn clean
  [filename]
  (as-> filename $
    (slurp $)
    (st/split $ #"\s+")))

(defn report-column->frequency
  "colnum uses zero-based indexing"
  [report colnum]
  (as-> report $
    (map #(nth % colnum) $)
    (frequencies $)))

(defn frequency->bit-data
  "bit-data gives the most-common and least-common bits"
  [frequency]
  (let [ones (frequency \1)
        zeroes (frequency \0)]
    (cond
      (nil? ones) {:most-common \0 :least-common \1}
      (nil? zeroes) {:most-common \1 :least-common \0}
      (> ones zeroes) {:most-common \1 :least-common \0}
      (> zeroes ones) {:most-common \0 :least-common \1}
      :else nil)))

(defn bit-datas->power-consumption
  [bit-datas]
  {:gamma-rate (Integer/parseInt (apply str (map :most-common bit-datas)) 2)
   :epsilon-rate (Integer/parseInt (apply str (map :least-common bit-datas)) 2)})

(defn submarine-power-consumption
  [report]
  (as-> report $
    (count (first report))
    (map (partial report-column->frequency report) (range $))
    (map frequency->bit-data $)
    (bit-datas->power-consumption $)
    (* (:gamma-rate $) (:epsilon-rate $))))

(defn submarine-rating
  [p? report]
  (loop [remaining-nums report
         colnum 0]
    (if (= (count remaining-nums) 1)
      (Integer/parseInt (first remaining-nums) 2)
      (let [freq (report-column->frequency remaining-nums colnum)]
        (recur (filter (partial p? freq colnum) remaining-nums)
               (inc colnum))))))

(defn oxygen-generation?
  [freq colnum str-num]
  (let [ones (freq \1)
        zeroes (freq \0)
        most-common (:most-common (frequency->bit-data freq))
        criteria (if (= ones zeroes) \1 most-common)]
    (= criteria
       (nth str-num colnum))))

(defn co2-scrubber?
  [freq colnum str-num]
  (let [ones (freq \1)
        zeroes (freq \0)
        least-common (:least-common (frequency->bit-data freq))
        criteria (if (= ones zeroes) \0 least-common)]
    (= criteria
       (nth str-num colnum))))

(defn submarine-life-support-rating
  [report]
  (let [times-to-filter (count (first report))
        oxy-gen-rating (submarine-rating oxygen-generation? report)
        scrubber-rating (submarine-rating co2-scrubber? report)]
    (* oxy-gen-rating scrubber-rating)))
