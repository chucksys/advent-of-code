(ns advent-of-code.2021.d7
  (:require [clojure.string :as st]))

(defn clean
  [filename]
  (as-> (slurp filename) $
    (st/split $ #"[,\n]")
    (map #(Integer/parseInt %) $)
    (sort $)))

(defn fuel-cost
  [crab-positions goal-position]
  (reduce + (map #(Math/abs (- % goal-position)) crab-positions)))

(defn minimum-fuel-cost
  [crab-positions]
  (let [uniq-positions (apply list (set crab-positions))
        fuel-costs (map (partial fuel-cost crab-positions) uniq-positions)]
    (apply min fuel-costs)))

(defn fuel-cost-for
  [a b]
  (let [n (Math/abs (- a b))]
    (/ (* n (inc n))
       2)))

(defn fuel-cost'
  [crab-positions goal-position]
  (reduce + (map (partial fuel-cost-for goal-position) crab-positions)))

(defn average
  [things]
  (int (/ (reduce + things)
          (count things))))

(defn positions-to-consider
  [crab-positions]
  (let [avg (average crab-positions)]
    (range (- avg 10) (+ avg 10))))

(defn minimum-fuel-cost'
  [crab-positions]
  (let [uniq-positions (positions-to-consider crab-positions)
        fuel-costs (map (partial fuel-cost' crab-positions) uniq-positions)]
    (apply min fuel-costs)))

