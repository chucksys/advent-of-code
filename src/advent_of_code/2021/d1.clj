(ns advent-of-code.2021.d1
  (:require [clojure.string :as st]))

(defn clean
  [filename]
  (map #(Integer/parseInt %) (-> filename
                                 (slurp)
                                 (st/split #"\s+"))))

(defn count-increasing-pairs
  [nums]
  (loop [nums nums
         acc 0]
    (if (<= (count nums) 1) acc
      (let [[x1 x2 & xs] nums]
        (if (< x1 x2) (recur (cons x2 xs) (inc acc))
          (recur (cons x2 xs) acc))))))

(defn count-increasing-threesums
  [nums]
  (loop [nums nums
         prev -1
         acc 0]
    (if (<= (count nums) 2) acc
      (let [[a b c & xs] nums
            sum (reduce + (list a b c))]
        (cond
          (= prev -1) (recur (cons b (cons c xs)) sum acc)
          (< prev sum) (recur (cons b (cons c xs)) sum (inc acc))
          :else (recur (cons b (cons c xs)) sum acc))))))
