(ns advent-of-code.2021.d5
  (:require [clojure.string :as st]))

(def line-regex #"(\d+),(\d+) -> (\d+),(\d+)")

(defn get-orientation
  [x y x' y']
  (cond
    (= x x') :vertical
    (= y y') :horizontal
    :else    :diagonal))

(defn line->line'
  [line]
  (let [[_ x y x' y'] (first (re-seq line-regex line))
        x (Integer/parseInt x)
        y (Integer/parseInt y)
        x' (Integer/parseInt x')
        y' (Integer/parseInt y')]
    {:x1 x :y1 y
     :x2 x' :y2 y'
     :orientation (get-orientation x y x' y')
     :offset (if (= y y') (- x' x) (- y' y))}))

(defn hori-or-vert?
  [{:keys [orientation]}]
  (not= orientation :diagonal))

(defn clean
  [filename]
  (as-> filename $
    (slurp $)
    (st/split $ #"\n+")
    (map line->line' $)
    (filter hori-or-vert? $)))

(defn clean2
  [filename]
  (as-> filename $
    (slurp $)
    (st/split $ #"\n+")
    (map line->line' $)))

(defn diagonal->points
  [x y x' y']
  (let [magnitude (Math/abs (- x x'))]
    (if (> x x')
      (diagonal->points x' y' x y)
      (if (< y y')
        ; go downwards \
        (map #(list (+ x %) (+ y %)) (range (inc magnitude)))
        ; go upwards /
        (map #(list (+ x %) (- y %)) (range (inc magnitude)))
        ))))

(defn line->points
  [{:keys [x1 y1 x2 y2 orientation offset]}]
  (cond
    (and (= orientation :horizontal) (pos? offset)) (map #(list (+ x1 %) y1) (range (inc offset)))
    (and (= orientation :horizontal) (neg? offset)) (map #(list (- x1 %) y1) (range (inc (- offset))))
    (and (= orientation :vertical) (pos? offset))   (map #(list x1 (+ y1 %)) (range (inc offset)))
    (and (= orientation :vertical) (neg? offset))   (map #(list x1 (- y1 %)) (range (inc (- offset))))
    (= orientation :diagonal)                       (diagonal->points x1 y1 x2 y2)))

(defn inc-with-nil
  [x]
  (if (nil? x)
    1
    (inc x)))

(defn draw-line
  [m line]
  (loop [[x & xs :as pts] (line->points line)
         m m]
    (if (empty? pts)
      m
      (recur xs (update m x inc-with-nil)))))

(defn draw-lines
  [lines]
  (loop [curr {}
         [line & xs :as ls] lines]
    (if (empty? ls)
      curr
      (recur (draw-line curr line) xs))))

(defn count-overlapping-points
  [lines]
  (let [m (draw-lines lines)]
    (count (filter (partial not= 1) (vals m)))))
