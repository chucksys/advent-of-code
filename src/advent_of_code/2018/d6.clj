(ns advent-of-code.2018.d6
  (:require [advent-of-code.utils :as utils])
  (:import (java.awt.image BufferedImage)
           (javax.imageio ImageIO)
           (java.io File)
           (java.awt Color)))

(def COORD #"(\d+), (\d+)")

(defn str->coord
  "Converts string to coordinate"
  [s]
  (let [m (rest (first (re-seq COORD s)))]
    {:x (read-string (first m))
     :y (read-string (second m))}))

(defn get-coords
  "Obtains list of coordinates :x :y"
  [fname]
  (->> fname
       utils/get-lines
       (map str->coord)))

(defn get-dimensions
  "Obtain minimum dimensions of list of coordinates"
  [coords]
  (let [max-x (apply max-key :x coords)
        max-y (apply max-key :y coords)]
    {:x (:x max-x) :y (:y max-y)}))

(defn d->dd
  "Converts 1D coordinates to 2D coordinates"
  [ind dim]
  {:x (mod ind (:x dim))
   :y (quot ind (:x dim))})

(defn dist
  "Manhatten distance from two points"
  [a b]
  (+ (Math/abs (- (:x a) (:x b)))
     (Math/abs (- (:y a) (:y b)))))

(defn min-key-ties
  "min-key, but replaces ties with '.' character"
  [k & more]
  (let [smallest (apply min (map k more))
        small-coords (filter (comp (partial = smallest) k) more)]
    (if (= 1 (count small-coords))
      (first small-coords)
      \.)))

(defn get-closest
  "Returns closest coordinate to 1D point"
  [coords dim ind]
  (apply min-key-ties (partial dist (d->dd ind dim)) coords))

(defn dd->d
  "Converts 2D coordinates to 1D coordinates"
  [dim coord]
  (+ (:x coord) (* (:x dim) (:y coord))))

(defn get-border-coords
  "Returns the boarder coordinates (coords along border)"
  [dim]
  (->> (concat (map #(hash-map :x % :y 0)              (range (:x dim)))
               (map #(hash-map :x 0 :y %)              (range (:y dim)))
               (map #(hash-map :x % :y (dec (:y dim))) (range (:x dim)))
               (map #(hash-map :x (dec (:x dim)) :y %) (range (:y dim))))
       set))

(defn populate-board
  "Populate board with all the things closest, manhatten distance"
  [coords]
  ; The board is 2 wider in every direction, to remove infinities
  (let [dim (get-dimensions coords)
        dim {:x (+ 2 (:x dim)) :y (+ 2 (:y dim))}
        coords (map #(hash-map :x (inc (:x %)) :y (inc (:y %))) coords)]
    (->> (* (:x dim) (:y dim))
         range
         (map (partial get-closest coords dim)))))

(defn rand-colour
  "Creates a random colour"
  []
  (Color. (rand-int 256) (rand-int 256) (rand-int 256)))

(defn output-board
  "Output the board to a file"
  [board dim fname]
  (let [cpairs (zipmap (set board) (repeatedly rand-colour))
        width  2
        bi (BufferedImage. (* (:x dim) width) (* (:y dim) width) BufferedImage/TYPE_INT_ARGB)
        g (.createGraphics bi)]
    (do
      (doseq [x (range (:x dim))
              y (range (:y dim))]
        (let [tile (nth board (dd->d dim {:x x :y y}))]
          (do
            (.setColor g (get cpairs tile))
            (.fillRect g (* x width) (* y width) width width))))
      (ImageIO/write bi "png" (File. fname)))))

(defn within-tot-dist?
  "Returns true if coord is within total distance of other coordinates"
  [coords x coord]
  (let [d (reduce + (map #(dist coord %) coords))]
    (< d x)))

(defn safe-region-size
  "Returns the size of the safety region"
  [coords]
  (let [dim (get-dimensions coords)
        board (range (* (:x dim) (:y dim)))
        board (filter #(within-tot-dist? coords 10000 (d->dd % dim)) board)]
    (count board)))
