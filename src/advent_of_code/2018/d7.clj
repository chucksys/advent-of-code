(ns advent-of-code.2018.d7
  (:require [advent-of-code.utils :as utils]
            [clojure.set :as cset]))

(def REE #"Step (\w) must be finished before step (\w) can begin.")

(defn line->instr
  "Converts a string to a requirement"
  [line]
  (let [got (rest (first (re-seq REE line)))]
    [(first got) (second got)]))

(defn get-reqs
  "Gets all instruction requirements (as list) from file"
  [fname]
  (->> (utils/get-lines fname)
       (map line->instr)))

(defn reqs->req-hash
  "Converts list of requirements to requirements list hash"
  [reqs]
  (let [__fn (fn [m [b4 after]] (assoc m after (sort (cons b4 (get m after [])))))
        reds (reduce __fn {} reqs)
        world (set (flatten (vals reds)))
        reqed (set (keys reds))
        unavail (cset/difference world reqed)
        doadd (fn [m i] (assoc m i '()))]
    (reduce doadd reds unavail)))

(defn find-first
  "Find the first step(s) that require no steps before it"
  [reqs-m]
  (->> (into [] reqs-m)
       (filter (comp empty? val))
       (map key)))

(defn remove-req
  "Remove single requirement from all other steps"
  [reqs-m req]
  (zipmap (keys reqs-m) (map #(remove (partial = req) %) (vals reqs-m))))

(defn left-avails
  "All available things that don't have any requirements (excluding ones done)"
  [reqs-m done]
  (-> (find-first reqs-m)
      set
      (cset/difference (set done))))

(defn dur
  "Duration of step X (only need to worry about A-Z steps)"
  [x]
  (list x (- (int (.charAt x 0)) 4)))

(defn map-ac
  [f l]
  (map (fn [[a b]] (list a (f b))) l))

(defn find-order
  "Find the order that everything should happen by"
  [reqs-m]
  (let [firsts (find-first reqs-m)
        max-workers 5]
    ; make sure todo is sorted!
    (loop [todo (sort firsts)
           m reqs-m
           done []
           elapsed -1000 ; because the first time you run, there are no actives
           ; which forces 1000 elapsed time added; do this to compensate
           actives []]
      (cond (and (empty? actives) (empty? todo))
            (list done (+ elapsed (apply max 0 (map second actives))))
            :else
            (let [avail-works (- max-workers (count actives))]
              (let [down-by (apply min 1000 (map second actives))
                    actives (map-ac #(- % down-by) actives)
                    done-ac (map first (filter (comp zero? second) actives))
                    new-m (reduce #(remove-req %1 %2) m done-ac)
                    new-done (concat done (sort done-ac))
                    actives (filter (comp not zero? second) actives)
                    doing-done (concat new-done (map first actives))
                    new-todo (sort (left-avails new-m doing-done))
                    avail-works (- max-workers (count actives))
                    rs (take avail-works new-todo)
                    new-todo (drop avail-works new-todo)
                    new-actives (concat actives (map dur rs))]
                (recur new-todo new-m new-done
                       (+ elapsed down-by) new-actives)))))))
