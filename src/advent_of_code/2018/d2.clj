(ns advent-of-code.2018.d2
  (:require [advent-of-code.utils :as utils]))

(defn part1
  "Does part 1"
  [fname]
  (let [counts (map (comp set vals frequencies)
                    (utils/get-lines fname))
        three (filter #(contains? % 3) counts)
        two (filter #(contains? % 2) counts)]
    (* (count three)
       (count two))))

; O(n^2)
(defn super-eq
  "Returns seq that is equal in seq, or false if not found"
  [lst]
  (loop [c (first lst)
         todo (rest lst)]
    (cond (empty? todo) false
          (not (nil? (some (partial = c) todo))) c
          :else (recur (first todo) (rest todo)))))

(defn remove-ith
  "Removes the ith item in a sequence and returns it"
  [lst i]
  (concat (subs lst 0 i)
          (subs lst (inc i))))

(defn part2
  "Does part 2"
  [fname]
  (let [lines (utils/get-lines fname)
        len (count (first lines))
        rem-app (fn [lst i] (map #(remove-ith % i) lst))
        i-to-try (map #(rem-app lines %) (range len))
        try-it (map super-eq i-to-try)]
    (filter (comp not false?) try-it)))
