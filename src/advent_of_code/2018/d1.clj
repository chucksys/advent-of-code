(ns advent-of-code.2018.d1
  (:require [advent-of-code.utils :as utils]))

(defn get-freqs
  "Obtains frequencies from a file"
  [fname]
  (map read-string (utils/get-lines fname)))

(defn reached-twice
  "Obtains the frequency that was reached twice"
  [freqs start]
  (loop [todo freqs
         done #{start}
         cur-sum start]
    (let [added (+ cur-sum (first todo))]
      (if (contains? done added)
        added
        (recur (rest todo) (conj done added) added)))))
