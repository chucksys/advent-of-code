(ns advent-of-code.2018.d4
  (:require [advent-of-code.utils :as utils]))

(def DFMT (java.text.SimpleDateFormat. "yyyy-MM-dd HH:mm"))

(def START-SHIFT #"\[(.*)\] Guard #(\d+) begins shift")
(def SLEEP #"\[(.*)\] falls asleep")
(def WAKE #"\[(.*)\] wakes up")

(defn create-entry
  "Create an entry from list"
  [l {id :id action :action}]
  (let [actual (rest (first l))
        dt (.parse DFMT (first actual))
        id (if (nil? id) (read-string (second actual)) id)]
    {:dt dt :id id :action action}))

(defn line->entry
  "Convert string to entry"
  [line id]
  (let [begins (re-seq START-SHIFT line)
        sleeps (re-seq SLEEP line)
        wakes  (re-seq WAKE line)]
    (cond (not (nil? begins)) (create-entry begins {:action :begin})
          (not (nil? sleeps)) (create-entry sleeps {:id id :action :sleep})
          :else               (create-entry wakes  {:id id :action :wake}))))

(defn get-stats
  "Obtain statuses of guards; assume list is in chrono order already"
  [fname]
  (let [lines (utils/get-lines fname)
        __fn (fn [{id :id acc :acc} line]
               (let [entry (line->entry line id)]
                 {:id (:id entry) :acc (conj acc entry)}))]
    (:acc (reduce __fn {:id nil :acc []} lines))))

(defn times->range
  "Convert dates to range of minutes; assume d1 < d2"
  [d1 d2]
  (let [h1 (.getHours d1)
        m1 (.getMinutes d1)
        m2 (.getMinutes d2)]
    (if (= h1 23)
      (concat (range m1 60) (range m2))
      (range m1 m2))))

(defn add-range
  "Add the range to a hash map"
  [prev curr m]
  (let [id (:id prev)
        rng (times->range (:dt prev) (:dt curr))]
    (loop [todo rng
           acc m]
      (if (empty? todo)
        acc
        (recur (rest todo)
               (update-in acc [id (first todo)]
                          #(inc (if (nil? %) 0 %))))))))

(defn stats->sleepmap
  "Convert stats to hash map of times asleep"
  [stats]
  (loop [prev (first stats)
         curr (second stats)
         todo (drop 2 stats)
         acc {}]
    (cond (empty? todo) acc

          (or (= (:action prev) :begin)
              (= (:action prev) :wake))
          (recur curr (first todo) (rest todo) acc)

          (= (:action prev) :sleep)
          (recur (first todo) (second todo) (drop 2 todo)
                 (add-range prev curr acc)))))

(defn sum-minutes
  "Add up minutes of guard given id and stats"
  [m id]
  (reduce + (vals (get m id))))

(defn add-minutes
  "Add up the number of minutes asleep for each guard and return that map"
  [m]
  (let [__fn (fn [_m k] (assoc-in _m [k :sum] (sum-minutes _m k)))]
    (reduce __fn m (keys m))))

(defn get-max
  "Find the maximum hour that a guard sleeps (only one); return hour and freq"
  [stats]
  (apply max-key (comp :sum second) (into [] stats)))
