(ns advent-of-code.2018.d5
  (:require [clojure.string :as str]))

(defn routine-replace
  "Removes all pairs (combinations) in string"
  [s pairs]
  (let [__fn (fn [acc p] (-> acc
                             (str/replace p "")
                             (str/replace (str (second p) (first p))
                                         "")))]
    (reduce __fn s pairs)))

(defn fully_react
  "Fully reacts all reactable pairs of letters"
  [polymer]
  (let [alphabet-pairs (map #(str % (str/upper-case %))
                            (set (str/lower-case polymer)))]
    (loop [old polymer]
      (let [newer (routine-replace old alphabet-pairs)]
        (if (= old newer)
          newer
          (recur newer))))))

(defn rem1-optimize
  "Removes 1 type of each character to check if reacting would get a smaller chain, and returns the length of the shortest chain"
  [polymer]
  (let [alphabet-pairs (set (str/lower-case polymer))
        rem1-ed (map #(str/replace (str/replace polymer (str %) "")
                                  (str/upper-case %)
                                  "")
                     alphabet-pairs)]
    (apply min (map (comp count fully_react) rem1-ed))))
