(ns advent-of-code.2018.d3
  (:require [advent-of-code.utils :as utils]))

; Claim: #<ID> @ x,y: wxh
(def claim-re #"#(\d+) @ (\d+),(\d+): (\d+)x(\d+)")
(def c-wid 1000)
(def c-hei 1000)

(defn line->claim
  "Create claim from string"
  [line]
  (let [items (rest (first (re-seq claim-re line)))]
    {:id (read-string (nth items 0))
     :x  (read-string (nth items 1))
     :y  (read-string (nth items 2))
     :w  (read-string (nth items 3))
     :h  (dec (read-string (nth items 4)))
     :d  (* (read-string (nth items 3))
            (read-string (nth items 4)))}))

(defn get-claims
  "Obtains claims as list from file"
  [fname]
  (->> (utils/get-lines fname)
       (map line->claim)))

(defn apply-line
  "Apply only a single line from a claim onto a cloth"
  [claim cl]
  (let [start (+ (:x claim) (* c-wid (+ (:y claim) (:h claim))))
        inds  (range start (+ start (:w claim)))]
    (reduce (fn [cl ind]
              (assoc cl ind (conj (get cl ind #{})
                                  (:id claim))))
            cl
            inds)))

(defn apply-claim
  "Apply a claim onto cloth map sets"
  [cl claim]
  ; do per line y-coordinate
  (loop [todo claim
         acc cl]
    (if (zero? (:h todo))
      (apply-line todo acc)
      (recur (update todo :h dec)
             (apply-line todo acc)))))

(defn apply-claims
  "Apply list of claims onto cloth map sets; return sq inches that have 2 or more claims"
  [lst cl]
  (let [app-claim (reduce apply-claim cl lst)
        things (map count (vals app-claim))]
    (count (filter (partial < 1) things))))

(defn claims
  "Find sets that contains id and return number of sets found"
  [id frqs]
  (get frqs #{id} 0))

(defn find-unoverlappings
  "Find claims that don't overlap with each other"
  [lst cl]
  (let [cl (vals (reduce apply-claim cl lst))
        valids (frequencies (filter #(= 1 (count %)) cl))
        ids (filter #(= (:d %1) (claims (:id %1) valids)) lst)]
    ids))

(def cloth {})
