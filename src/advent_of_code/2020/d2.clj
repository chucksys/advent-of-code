(ns advent-of-code.2020.d2)

(def re-line-fmt #"(\d+)-(\d+) (.): (.+)")

(defn ->record
  [[_ lo hi c password]]
  {:lo (Integer/parseInt lo)
   :hi (Integer/parseInt hi)
   :ch (first (char-array c))
   :pw password})

(defn clean
  [filename]
  (->> filename
       (slurp)
       (re-seq re-line-fmt)
       (map ->record)))

(defn valid?
  [{:keys [lo hi ch pw]}]
  (<= lo
      (count (filter (partial = ch) pw))
      hi))

(defn valid2?
  [{:keys [lo hi ch pw]}]
  (let [lo (nth pw (dec lo))
        hi (nth pw (dec hi))]
    (and (or (= ch lo)
             (= ch hi))
         (not= lo hi))))
