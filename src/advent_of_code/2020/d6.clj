(ns advent-of-code.2020.d6
  (:require [clojure.string :as s]
            [clojure.set :as cset]))

(defn clean-group
  [txt]
  (let [answers (s/split txt #"\n")]
    (apply cset/union (map set answers))))

(defn clean-groups
  [groups]
  (map clean-group groups))

(defn clean
  [filename]
  (-> (slurp filename)
      (s/split #"\n\n")
      (clean-groups)))

(defn clean-group'
  [txt]
  (let [answers (s/split txt #"\n")]
    (apply cset/intersection (map set answers))))

(defn clean-groups'
  [groups]
  (map clean-group' groups))

(defn clean'
  [filename]
  (-> (slurp filename)
      (s/split #"\n\n")
      (clean-groups')))
