(ns advent-of-code.2020.d4
  (:require [clojure.string :as s]))

(defn pair->attr
  [pair]
  (let [[attr v] (s/split pair #":")]
    {(keyword attr) v}))

(defn line->rec
  [line]
  (let [pairs (s/split line #"\s+")]
    (apply merge (map pair->attr pairs))))

(defn ->record
  [passport-text]
  (let [lines (s/split passport-text #"\n")]
    (apply merge (map line->rec lines))))

(defn ->records
  [passport-lines]
  (map ->record passport-lines))

(defn clean
  [filename]
  (-> (slurp filename)
      (s/split #"\n\n")
      (->records)))

(def correct-keys
  '(#{:byr :iyr :eyr :hgt :hcl :ecl :pid :cid}
    #{:byr :iyr :eyr :hgt :hcl :ecl :pid}))

(defn valid?
  [rec]
  (let [key-set (set (keys rec))]
    (if (not (or (= (first correct-keys) key-set)
                 (= (second correct-keys) key-set)))
      false
      true)))

(def year-re #"\d{4}")
(def height-re #"(\d+)(cm|in)")
(def hair-colour #"#[0-9a-f]{6}")
(def eye-colour #{:amb :blu :brn :gry :grn :hzl :oth})
(def pass-id #"\d{9}")

(defn valid-year?
  [yr]
  (not (nil? (re-matches year-re yr))))

(defn valid-height?
  [hgt]
  (let [[_ v unit :as h] (re-matches height-re hgt)]
    (if (nil? h)
      false
      (if (= unit "cm")
        (<= 150 (Integer/parseInt v) 193)
        (<= 59 (Integer/parseInt v) 76)))))

(defn valid-colour?
  [colour]
  (not (nil? (re-matches hair-colour colour))))

(defn valid-eyecolour?
  [ecl]
  (contains? eye-colour (keyword ecl)))

(defn valid-pid?
  [pid]
  (not (nil? (re-matches pass-id pid))))

(defn valid2?
  [{:keys [byr iyr eyr hgt hcl ecl pid cid]}]
  (and (not (nil? byr)) (valid-year? byr) (<= 1920 (Integer/parseInt byr) 2002)
       (not (nil? iyr)) (valid-year? iyr) (<= 2010 (Integer/parseInt iyr) 2020)
       (not (nil? eyr)) (valid-year? eyr) (<= 2020 (Integer/parseInt eyr) 2030)
       (not (nil? hgt)) (valid-height? hgt)
       (not (nil? hcl)) (valid-colour? hcl)
       (not (nil? ecl)) (valid-eyecolour? ecl)
       (not (nil? pid)) (valid-pid? pid)))
