(ns advent-of-code.2020.d1
  (:require [clojure.string :as s]
            [clojure.math.combinatorics :as comb]))

(defn clean
  [filename]
  (map #(Integer/parseInt %) (-> filename
                                 (slurp)
                                 (s/split #"\s+"))))

(defn find-npair-for-sum
  [nums n target]
  (some #(when (= target (apply + %)) %) (comb/combinations nums n)))

(defn find-pair-for-sum
  [nums target]
  (find-npair-for-sum nums 2 target))
