(ns advent-of-code.2020.d5
  (:require [clojure.string :as s]))

(defn clean-line
  [line]
  (map (comp symbol str) line))

(defn clean-lines
  [lines]
  (map clean-line lines))

(defn clean
  [filename]
  (-> (slurp filename)
      (s/split #"\n")
      (clean-lines)))

(defn get-sid
  [r c]
  (+ c (* 8 r)))

(defn -row-lo
  [row-lo row-hi ins]
  (if (= 'B ins)
    (quot (+ row-lo row-hi) 2)
    row-lo))

(defn -row-hi
  [row-lo row-hi ins]
  (if (= 'F ins)
    (quot (+ row-lo row-hi) 2)
    row-hi))

(defn -col-lo
  [col-lo col-hi ins]
  (if (= 'R ins)
    (quot (+ col-lo col-hi) 2)
    col-lo))

(defn -col-hi
  [col-lo col-hi ins]
  (if (= 'L ins)
    (quot (+ col-lo col-hi) 2)
    col-hi))

(defn process-pass
  [instructions]
  (loop [[x & xs :as ins] instructions
         row-lo 0
         row-hi 128
         col-lo 0
         col-hi 8]
    (if (empty? ins)
      {:instructions instructions
       :row row-lo
       :col col-lo
       :sid (get-sid row-lo col-lo)}
      (recur
        xs
        (-row-lo row-lo row-hi x)
        (-row-hi row-lo row-hi x)
        (-col-lo col-lo col-hi x)
        (-col-hi col-lo col-hi x)))))
