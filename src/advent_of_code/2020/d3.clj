(ns advent-of-code.2020.d3
  (:require [clojure.string :as s]))

(defn clean
  [filename]
  (let [lines (-> filename
                  (slurp)
                  (s/split #"\n"))]
    (map (comp cycle (partial map #(= \# %))) lines)))

(defn next-pos
  [[cx cy] [sx sy]]
  [(+ cx sx) (+ cy sy)])

(defn tree?
  [[x y] area]
  (nth (nth area y) x))

(defn traverse
  [area step]
  (let [ending-pos (dec (count area))]
    (loop [[_ cy :as c] '(0 0)
           trees 0]
      (let [n (next-pos c step)]
        (if (= cy ending-pos)
          trees
          (recur n (+ trees (if (tree? n area) 1 0))))))))
