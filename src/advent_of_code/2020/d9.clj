(ns advent-of-code.2020.d9
  (:require [clojure.string :as s]
            [clojure.math.combinatorics :as comb]))

(defn clean
  [filename]
  (map #(Long/valueOf %) (s/split (slurp filename) #"\n")))

(defn valid?
  [preamble n]
  (loop [[[a b] & xs :as l] (comb/combinations preamble 2)]
    (cond
      (empty? l) false
      (= (+ a b) n) true
      :else (recur xs))))

(defn find-invalid-number
  [nums]
  (loop [nums nums]
    (let [preamble (take 25 nums)
          current (nth nums 25 0)]
      (cond
        (empty? nums) nil
        (valid? preamble current) (recur (rest nums))
        :else current))))

(defn find-continguous-sum
  "Use sliding window approach, because you have to."
  [nums n]
  (loop [window '()
         [f & rs :as nums] nums]
    (let [sum (apply + 0 window)]
      (cond
        (empty? nums) nil
        (= sum n) window
        (> sum n) (recur (drop-last window)
                         nums)
        (< sum n) (recur (cons f window)
                         rs)))))
