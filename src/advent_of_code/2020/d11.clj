(ns advent-of-code.2020.d11
  (:require [clojure.string :as s]
            [clojure.math.combinatorics :as comb]))

; 0 - a tile without any chairs
; 1 - a tile with an empty chair
; 2 - a tile with a filled chair
; :data is a map of row-indexed coordinates ind = y * w + x
(def sample-object {:w 10 :h 10 :data []})

(defn process-lines
  [lines]
  (let [width (count (first lines))
        height (count lines)
        data (into [] (map (comp vec seq) lines))]
    {:w width :h height :data data}))

(defn clean
  [filename]
  (process-lines (s/split (slurp filename) #"\n")))

(defn get-at
  [x y data]
  (get-in data [y x] \.))

(defn assoc-at
  [x y data v]
  (assoc-in data [y x] v))

(defn num-person-in-index
  [x y olddata]
  (if (= \# (get-at x y olddata))
    1
    0))

(defn neighbours
  [x y olddata]
  (let [perms (comb/cartesian-product [-1 0 1] [-1 0 1])]
    (loop [[[dx dy] & xs :as l] perms
           acc 0]
      (cond (empty? l) acc
            (= dx dy 0) (recur xs acc)
            :else (recur xs (+ acc (num-person-in-index (+ x dx) (+ y dy) olddata)))))))

(defn update-data-at-index
  [x y olddata newdata]
  (let [seat (get-at x y olddata)
        num-neighbours (neighbours x y olddata)]
    (cond
      (and (= seat \L) (= num-neighbours 0)) (assoc-at x y newdata \#)
      (and (= seat \#) (>= num-neighbours 4)) (assoc-at x y newdata \L)
      :else newdata)))

(defn step
  [{:keys [w h data]}]
  (loop [[[x y] & xs :as l] (comb/cartesian-product (range w) (range h))
         newdata data]
    (cond (empty? l) {:w w :h h :data newdata}
          :else (recur xs (update-data-at-index x y data newdata)))))

(defn step-until-stable
  [data]
  (loop [olddata nil
         newdata data]
    (if (not= olddata newdata)
      (recur newdata (step newdata))
      newdata)))

(defn count-things
  [{:keys [data]}]
  (->> data
       (apply concat)
       (frequencies)))

(defn pp
  [{:keys [data]}]
  (->> data
       (map (partial s/join))
       (s/join "\n")))
