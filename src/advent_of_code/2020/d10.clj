(ns advent-of-code.2020.d10
  (:require [clojure.string :as s]))

(defn clean
  [filename]
  (let [adapters (cons 0 (sort (map #(Long/valueOf %) (s/split (slurp filename) #"\n"))))
        built-in (+ 3 (last adapters))]
    (conj (vec adapters) built-in)))

(defn parse-differences
  [adapters]
  (loop [[a b & rst :as l] adapters
         acc {:zero 0 :one 0 :two 0 :three 0}]
    (cond
      (or (empty? l) (= 1 (count l))) acc
      (= 0 (- b a)) (recur (cons b rst) (update acc :zero inc))
      (= 1 (- b a)) (recur (cons b rst) (update acc :one inc))
      (= 2 (- b a)) (recur (cons b rst) (update acc :two inc))
      (= 3 (- b a)) (recur (cons b rst) (update acc :three inc))
      :else "error")))

(defn ways-to-do-it
  "Dynamic programming saves the day yet again!!! Runs O(n), basically."
  [adapters]
  (loop [[x & xs :as l] (rest (reverse adapters))
         acc {(last adapters) 1}
         last-added 1]
    (cond
      (empty? l) last-added
      :else (let [v (+ (get acc (+ x 1) 0)
                       (get acc (+ x 2) 0)
                       (get acc (+ x 3) 0))]
              (recur xs (assoc acc x v) v)))))
