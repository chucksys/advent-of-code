(ns advent-of-code.2020.d8
  (:require [clojure.string :as s]))

(defn parse-line
  [line]
  (let [[instr op] (s/split line #"\s+")]
    (cond
      (= instr "nop") [:nop (Integer/parseInt op)]
      (= instr "acc") [:acc (Integer/parseInt op)]
      (= instr "jmp") [:jmp (Integer/parseInt op)])))

(defn clean
  [filename]
  (into [] (map parse-line (s/split (slurp filename) #"\n"))))

(defn get-next-pc
  [instrs pc]
  (let [[instr op] (nth instrs pc)]
    (cond
      (= instr :nop) (inc pc)
      (= instr :acc) (inc pc)
      (= instr :jmp) (+ pc op))))

(defn get-next-acc
  [instrs pc acc]
  (let [[instr op] (nth instrs pc)]
    (cond
      (= instr :nop) acc
      (= instr :acc) (+ acc op)
      (= instr :jmp) acc)))

(defn interpret-until-already-seen
  [instrs]
  (loop [pc 0
         acc 0
         seen #{0}]
    (if (= pc (count instrs))
      [false acc]
      (let [next-pc (get-next-pc instrs pc)
            next-acc (get-next-acc instrs pc acc)]
        (if (contains? seen next-pc)
          [true acc]
          (recur next-pc next-acc (conj seen next-pc)))))))

(defn can-flip-instr?
  [instrs at]
  (let [[instr _] (nth instrs at)]
    (or (= instr :nop) (= instr :jmp))))

(defn flip-instr
  [instrs at]
  (let [[instr op] (nth instrs at)]
    (if (= instr :nop)
      (assoc instrs at [:jmp op])
      (assoc instrs at [:nop op]))))

(defn instrs-terminate?
  [instrs]
  (false? (first (interpret-until-already-seen instrs))))

(defn get-non-terminating-acc
  [instrs]
  (let [can-do-it (filter (partial can-flip-instr? instrs) (range (count instrs)))
        modded-instrs (map (partial flip-instr instrs) can-do-it)]
    (loop [[instrs & xs :as iall] modded-instrs]
      (cond
        (empty? iall) false
        (instrs-terminate? instrs) (interpret-until-already-seen instrs)
        :else (recur xs)))))
