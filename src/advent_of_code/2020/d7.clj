(ns advent-of-code.2020.d7
  (:require [clojure.string :as s]))

(def rule0 {"light red bag" {"bright white bag" 1
                             "muted yellow bag" 2}})

(defn make-rule-from-body
  [bag-name rule-body]
  (let [rule-body (s/replace (s/replace rule-body "." "") "bags" "bag")
        bag-name (s/replace bag-name "bags" "bag")
        body-bags (s/split rule-body #", ")
        parse-bag (fn [txt] (let [[_ n col] (re-matches #"(\d+) (.+)" txt)]
                              {col (Integer/parseInt n)}))]
    {bag-name (into {} (map parse-bag body-bags))}))

(defn clean-rule
  [line]
  (let [[bag-name rule-body] (s/split line #" contain ")]
    (if (= rule-body "no other bags.")
      {bag-name []}
      (make-rule-from-body bag-name rule-body))))

(defn clean
  [filename]
  (map clean-rule (s/split (slurp filename) #"\n")))

(def can-hold?
  "Asks the question 'Can bag-name hold target somewhere down the line?'"
  (memoize (fn [bags target bag-name]
             (let [inside (get bags bag-name {})]
               (cond
                 (empty? inside) false
                 (contains? inside target) true
                 :else (some true?
                             (map (partial can-hold? bags target) (keys inside))))))))

(def count-bags
  "How many bags are inside your bag? This includes the current bag itself"
  (memoize
    (fn [bags target]
      (let [inside (get bags target {})
            inside-bag-costs (into {} (map #(identity {% (count-bags bags %)})
                                           (keys inside)))]
        (apply + 1 (map #(* (get inside-bag-costs %)
                            (get inside %))
                        (keys inside)))))))
