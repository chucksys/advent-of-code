(ns advent-of-code.utils
  (:require [clojure.string :as s]))

(defn resource-name
  "Get filename of resource"
  [year day]
  (format "resources/input%s-d%s" year day))

(defn get-lines
  "Gets the lines in a file"
  [fname]
  (s/split (slurp fname) #"\n"))

(defn get-resource
  "Gets the lines of a resource."
  [year day]
  (get-lines (resource-name year day)))

(defn count-if
  [p coll]
  (reduce (fn [acc x] (if (p x) (inc acc) acc)) 0 coll))

(defn update-lots
  [m ks f]
  (loop [[k & ks] ks
         m m]
    (if (nil? k)
      m
      (recur ks (update m k f)))))
