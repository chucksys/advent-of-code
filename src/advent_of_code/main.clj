(ns advent-of-code.main
  (:require [babashka.curl :as curl]
            [environ.core :refer [env]]
            [clojure.java.io :as io])
  (:use [clojure.pprint :only [pprint]]))

(defn -main
  "Get input file over command line."
  [& args]

  (assert (= 2 (count args)) "Expected 2 arguments: <year> <day>")

  (let [year (Integer/parseInt (first args))
        day (Integer/parseInt (second args))
        filename (format "resources/input%d-d%d" year day)
        url (format "https://adventofcode.com/%d/day/%d/input" year day)
        resp (curl/get url {:headers {:cookie (str "session=" (env :session))}})]
    (do
      (printf "Getting Advent Of Code puzzle input for %d day %d.\n" year day)
      (if (.exists (io/file filename))
        (printf "Resource file `%s` already exists. Overwriting.\n" filename)
        (printf "Writing to resource file `%s`.\n" filename))
      (spit filename (:body resp))
      (printf "Finished. %d bytes written." (count (:body resp))))))
