(ns advent-of-code.2019.d3
  (:require [clojure.string :as str]))

(def INSTR-R #"^(U|D|L|R)(\d+)$")

(defn process-instr
  "Convert an instruction (e.g. R12) into {:dir :R :ts 12}"
  [instr]
  (let [m (rest (first (re-seq INSTR-R instr)))]
    {:dir (keyword (first m)) :ts (read-string (second m))}))

(defn process-instr-list
  "Convert a string of wire instruction into a list of {:dir :ts}"
  [wire-line]
  (let [wire-line (str/split (.trim wire-line) #",")]
    (vec (map process-instr wire-line))))

(defn clean
  "Convert input filename into list of list of instructions"
  [filename]
  (let [content (.trim (slurp filename))
        wires (str/split content #"\n")]
    (->> wires
         (map process-instr-list)
         vec)))

(defn handle-coord
  "If a coordinate is already inside the hashmap, we update it. If not, we just add it in. Used in update function"
  [v]
  (if (nil? v) 1 (inc v)))

(defn get-nonoffset-coords
  "Convert an instruction into a set of coordinates starting at {:x 1 :y 0}, depending on what direction you are going"
  [{dir :dir ts :ts}]
  (cond (= dir :U) (map #(hash-map :x 0 :y %) (range -1 (dec (- 0 ts)) -1))
        (= dir :D) (map #(hash-map :x 0 :y %) (range 1 (inc ts)))
        (= dir :L) (map #(hash-map :x % :y 0) (range -1 (dec (- 0 ts)) -1))
        (= dir :R) (map #(hash-map :x % :y 0) (range 1 (inc ts)))
        :else '()))

(defn get-line-coords
  "Convert an instruction into a set of coordinates {:x :y}"
  [{from-x :x from-y :y} instr]
  (map (fn [{x :x y :y}] {:x (+ x from-x) :y (+ y from-y)})
       (get-nonoffset-coords instr)))

(defn update-coord
  "Update coordinate using an instruction"
  [{x :x y :y} {dir :dir ts :ts}]
  (cond (= dir :U) {:x x :y (- y ts)}
        (= dir :D) {:x x :y (+ y ts)}
        (= dir :L) {:x (- x ts) :y y}
        (= dir :R) {:x (+ x ts) :y y}
        :else {:x x :y y}))

(defn follow-instrs
  "Convert a set of instructions FOR A SINGLE WIRE into a set of coordinates on a cartesian grid, starting at origin"
  [coords instrs]
  (let [helper (fn [f u xs starting]
                 (loop [acc '()
                        curr starting
                        [x & xs :as l] xs]
                   (if (empty? l)
                     acc
                     (recur (concat acc (f curr x)) (u curr x) xs))))]
    (loop [converted-coords (helper get-line-coords update-coord instrs {:x 0 :y 0})
           coords coords]
      (if (empty? converted-coords)
        coords
        (recur (rest converted-coords) (update coords (first converted-coords) handle-coord))))))

(defn manhatten
  "Returns the manhatten distance given a cartesian coordinate"
  [{x :x y :y}]
  (+ (Math/abs x) (Math/abs y)))

(defn part1
  "Finish part 1"
  [wires]
  (let [[w1 w2] (map (partial follow-instrs {}) wires)]
    (if (= 2 (count wires))
      (->> (filter #(contains? w1 %) (keys w2))
           (map manhatten)
           sort
           first)
      0)))

(defn coord-sandwiched-between?
  "Returns true if the coordinate is sandwiched between the two other coordinates"
  [{dx :x dy :y} {upx :x upy :y} {x :x y :y}]
  (cond (= dx upx x) (or (<= dy y upy) (>= dy y upy))
        (= dy upy y) (or (<= dx x upx) (>= dx x upx))
        :else false))

(defn coord-sub
  "Subtracts coordinates"
  [{ax :x ay :y} {bx :x by :y}]
  {:x (- ax bx) :y (- ay by)})

(defn get-steps
  "Get the number of steps to get from origin to coordinate on wire"
  [wire-instrs coord]
  (loop [curr {:x 0 :y 0}
         steps 0
         [instr & instrs :as wire-instrs] wire-instrs]
    (if (empty? wire-instrs)
      0
      (let [next (update-coord curr instr)]
        (if (coord-sandwiched-between? curr next coord)
          (+ steps (manhatten (coord-sub coord curr)))
          (recur next (+ steps (:ts instr)) instrs))))))

(defn part2
  "Finish part 2"
  [[instr1 instr2 :as wires]]
  (let [[w1 w2] (map (partial follow-instrs {}) wires)
        intersect-pts (filter #(contains? w1 %) (keys w2))]
    (->> (map + (map (partial get-steps instr1) intersect-pts) (map (partial get-steps instr2) intersect-pts))
         sort
         first)))