(ns advent-of-code.2019.d5
  (:require [clojure.string :as str]))

(defn clean
  "Convert input file name into list of numbers"
  [filename]
  (let [content (.trim (slurp filename))
        splitted (str/split content #",\s*")]
    (-> (map read-string splitted)
        vec)))

(defn noun-verb
  "Add nouns and verbs to input"
  [input noun verb]
  (-> input
      (assoc 1 noun)
      (assoc 2 verb)))

(def INSTR-SIZE
  {1 4, 2 4, 3 2, 4 2, 5 3, 6 3, 7 4, 8 4, 99 1})

(defn explode-opcode
  "Explodes an opcode from a single number into 3 modes and an instruction.
  Modes are either (I)mmediate or (P)osition (single characters)."
  [opcode]
  (let [s (format "%05d" opcode)]
    [(if (= \0 (nth s 0)) :pos :imm)
     (if (= \0 (nth s 1)) :pos :imm)
     (if (= \0 (nth s 2)) :pos :imm)
     (Integer/parseInt (subs s 3))]))

(defn add-instr
  "Handles the add instruction."
  [[code index] [mode3 mode2 mode1 opcode] [arg1 arg2 arg3]]
  (let [arg1 (if (= :pos mode1) (nth code arg1) arg1)
        arg2 (if (= :pos mode2) (nth code arg2) arg2)]
    [(assoc code arg3 (+ arg1 arg2)) (+ index 4)]))

(defn mul-instr
  "Handles the multiply instruction."
  [[code index] [mode3 mode2 mode1 opcode] [arg1 arg2 arg3]]
  (let [arg1 (if (= :pos mode1) (nth code arg1) arg1)
        arg2 (if (= :pos mode2) (nth code arg2) arg2)]
    [(assoc code arg3 (* arg1 arg2)) (+ index 4)]))

(defn input-instr
  "Handles the input instruction. Can be tested by enclosing in (with-in-str s)"
  [[code index] [mode3 mode2 mode1 opcode] [arg1]]
  [(assoc code arg1 (read-string (.trim (read-line)))) (+ index 2)])

(defn output-instr
  "Handles the output instruction."
  [[code index] [mode3 mode2 mode1 opcode] [arg1]]
  (print (format "%d " (if (= :pos mode1) (nth code arg1) arg1)))
  [code (+ index 2)])

(defn jump-if-true-instr
  "Handles jump if true instruction."
  [[code index] [mode3 mode2 mode1 opcode] [arg1 arg2]]
  (let [arg1 (if (= :pos mode1) (nth code arg1) arg1)
        arg2 (if (= :pos mode2) (nth code arg2) arg2)]
    [code (if (not (zero? arg1)) arg2 (+ index 3))]))

(defn jump-if-false-instr
  "Handles jump if false instruction."
  [[code index] [mode3 mode2 mode1 opcode] [arg1 arg2]]
  (let [arg1 (if (= :pos mode1) (nth code arg1) arg1)
        arg2 (if (= :pos mode2) (nth code arg2) arg2)]
    [code (if (zero? arg1) arg2 (+ index 3))]))

(defn less-instr
  "Handles less than instruction."
  [[code index] [mode3 mode2 mode1 opcode] [arg1 arg2 arg3]]
  (let [arg1 (if (= :pos mode1) (nth code arg1) arg1)
        arg2 (if (= :pos mode2) (nth code arg2) arg2)]
    [(assoc code arg3 (if (< arg1 arg2) 1 0)) (+ index 4)]))

(defn equals-instr
  "Handles equals instruction."
  [[code index] [mode3 mode2 mode1 opcode] [arg1 arg2 arg3]]
  (let [arg1 (if (= :pos mode1) (nth code arg1) arg1)
        arg2 (if (= :pos mode2) (nth code arg2) arg2)]
    [(assoc code arg3 (if (= arg1 arg2) 1 0)) (+ index 4)]))

; 1, a, b, a + b
; 2, a, b, a * b
; 3, save_ind
; 4, output_ind
; 99
(defn execute-instruction
  "Execute a single instruction and return state"
  [state [opcode & args]]
  (let [[mode3 mode2 mode1 opcode :as exploded] (explode-opcode opcode)]
    (cond (= 1 opcode) (add-instr state exploded args)
          (= 2 opcode) (mul-instr state exploded args)
          (= 3 opcode) (input-instr state exploded args)
          (= 4 opcode) (output-instr state exploded args)
          (= 5 opcode) (jump-if-true-instr state exploded args)
          (= 6 opcode) (jump-if-false-instr state exploded args)
          (= 7 opcode) (less-instr state exploded args)
          (= 8 opcode) (equals-instr state exploded args)
          (= 99 opcode) :end)))

(defn execute-code
  "Execute all instructions until the code terminates"
  [code]
  (loop [[code index :as state] [code 0]]
    (let [[m3 m2 m1 oc] (explode-opcode (nth code index))
          size (get INSTR-SIZE oc)]
      (if (= 99 oc)
        state
        (recur (execute-instruction state (take size (drop index code))))))))

(defn part1
  "Do the do"
  [input]
  (execute-code input))

(def part2 part1)
