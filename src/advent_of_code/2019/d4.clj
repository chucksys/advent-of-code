(ns advent-of-code.2019.d4
  (:require [clojure.string :as str]))

(defn same-adj-digits?
  "Checks that a string has at least 1 pair of digits that are the same"
  [pwd]
  (or (= (nth pwd 0) (nth pwd 1))
      (= (nth pwd 1) (nth pwd 2))
      (= (nth pwd 2) (nth pwd 3))
      (= (nth pwd 3) (nth pwd 4))
      (= (nth pwd 4) (nth pwd 5))))

(defn never-decrease?
  "Checks that all digits of a string never decrease"
  [pwd]
  (->> (sort pwd)
       str/join
       (= pwd)))

(defn valid-password?
  "Checks that a password is valid, returns true if it is"
  [pwd]
  (cond (not= 6 (count pwd)) false
        (not (same-adj-digits? pwd)) false
        (not (never-decrease? pwd)) false
        :else true))

(defn part1
  "Does part 1"
  [rng]
  (->> (filter (comp valid-password? str) rng)
       count))

(defn wtf-adjacency?
  "Checks that a password has 1 digit twice repeating"
  [pwd]
  (->> (frequencies pwd)
       vals
       (some (partial = 2))))

(defn part2
  "Does part 2"
  [rng]
  (->> (filter (comp #(and (valid-password? %) (wtf-adjacency? %)) str) rng)
       count))