(ns advent-of-code.2019.d2
  (:require [clojure.string :as str]))

(defn clean
  "Convert input file name into list of numbers"
  [filename]
  (let [content (.trim (slurp filename))
        splitted (str/split content #",\s*")]
    (-> (map read-string splitted)
        vec)))

(defn noun-verb
  "Add nouns and verbs to input"
  [input noun verb]
  (-> input
      (assoc 1 noun)
      (assoc 2 verb)))

(def opcode
  {1 +
   2 *
   99 +})

(defn part1
  "Have at it!"
  [input noun verb]
  (loop [index 0
         input (noun-verb input noun verb)]
    (let [instruction (nth input index)
          num1 (nth input (nth input (+ 1 index)) 0)
          num2 (nth input (nth input (+ 2 index)) 0)
          out_index (nth input (+ 3 index))
          result ((opcode instruction) num1 num2)]
      (if (= instruction 99)
        (nth input 0)
        (recur (+ 4 index) (assoc input out_index result))))))

(defn force-verb
  "Brute force the verb given that the noun is known"
  [input noun x]
  (loop [verb 0]
    (if (= verb (count input))
      -1
      (if (= x (part1 input noun verb))
          verb
          (recur (inc verb))))))

(defn part2
  "Attempt to brute force what would get me x"
  [input x]
  (loop [noun 0]
    (if (= noun (count input))
      [-1 -1]
      (let [found-verb (force-verb input noun x)]
        (if (= -1 found-verb)
          (recur (inc noun))
          [noun found-verb])))))
