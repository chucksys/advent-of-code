(ns advent-of-code.2019.d6
  (:require [ubergraph.core :as uber]
            [ubergraph.alg :as uber-alg]
            [clojure.string :as s]))

(defn clean1
  "Convert the input file name into a tree (which is a graph, directed)"
  [filename]
  (loop [[x & xs :as l] (s/split (slurp filename) #"\s+")
         g (uber/graph)]
    (if (empty? l)
      g
      (let [[a b] (s/split x #"\)")]
        (recur xs (uber/add-directed-edges g [(keyword a) (keyword b)]))))))

(defn clean2
  "Convert the input file name into a tree (which is a graph, bidirectional)"
  [filename]
  (loop [[x & xs :as l] (s/split (slurp filename) #"\s+")
         g (uber/graph)]
    (if (empty? l)
      g
      (let [[a b] (s/split x #"\)")]
        (recur xs (uber/add-edges g [(keyword a) (keyword b)]))))))

(defn total-children
  "Find the total number of children of a node"
  [g node]
  (loop [[node & xs :as nodes] [node]
         sum 0]
    (if (empty? nodes)
      sum
      (let [immediate-childs (map :dest (uber/find-edges g {:src node}))]
        (recur (concat xs immediate-childs) (+ sum (count immediate-childs)))))))

(defn part1
  "Finish part 1"
  [g]
  (->> (uber/nodes g)
       (map (partial total-children g))
       (reduce + 0)))

(defn part2
  "Finish part2"
  [g2]
  (->> (uber-alg/shortest-path g2 :YOU :SAN)
       (:cost)
       (+ -2)))
